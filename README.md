# Homework assignment

<h3>Installing</h3>
<ol>
    <li>
        <p>Clone the Git repository to the desired location:</p>
        <div>
            <pre>
                <code> 
                    git clone https://gitlab.com/nazrana/assignment-ps.git 
                    cd assignemnt-ps
                </code>
            </pre>
        </div>
    </li>
    <li>
        <p>Run composer to install the dependencies and generate the auto-loader:</p>
        <div>
            <pre>
                <code> 
                    composer install
                </code>
            </pre>
        </div>
    </li>
<ol>
<h3>Testing</h3>
<ul>
    <li>
        <p>Run following:</p>
        <div>
            <pre>
                <code> 
                    composer run test
                </code>
            </pre>
        </div>
    </li>
<ul>
<h3 style="color:red">**input.csv file is available in root with sample data</h3>
