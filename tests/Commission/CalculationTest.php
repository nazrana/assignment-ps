<?php
    use PHPUnit\Framework\TestCase;
 
    class CalculationTest extends TestCase {
 
        public function testIsSumCorrect(){
            $calc = new Calculation();
            $result = $calc->commisionOnDeposit(1200);
            $expected = 0.36;
            $this->assertSame($expected,$result);       
        }
 
    }
    