<?php

declare(strict_types=1);

class Calculation {
    public function calculate() {
        $_SESSION['count'] = 0;
        $_SESSION['week'] = 1;
        $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
        fgetcsv($csvFile);
        $matches = [];
        while (($line = fgetcsv($csvFile)) !== false) {
            if (preg_match('/(.+),.+,(.+)/', $line[0], $matches)) {
                $values = explode(',', $matches[0]);
                $operationDate = $values[0];
                $userId = $values[1];
                $userType = $values[2];
                $operationType = $values[3];
                $operationAmount = $values[4];
                $operationCurrency = $values[5];
                $obj = new Calculation();
                if ($operationType == 'deposit') {
                    $commission = $obj->commisionOnDeposit($operationAmount);
                } elseif ($operationType == 'withdraw') {
                    $commission = $obj->commisionOnWithdraw($userType, $operationDate, $operationAmount, $operationCurrency);
                }
                echo '<p>'.$commission.'</p>';
            }
        }
    }

    /**
     * Calculate commission on deposit.
     *
     * @return float
     */
    public function commisionOnDeposit($operationAmount) {
        $commission = $operationAmount * 0.03 / 100;
        return $commission;
    }

    /**
     * Calculate commission on withdraw.
     *
     * @return float
     */
    public function commisionOnWithdraw($userType, $operationDate, $operationAmount, $operationCurrency) {
        if ($userType == 'business') {
            $commission = $operationAmount * 0.5 / 100;
        } elseif ($userType == 'private') {
            $obj = new Calculation();
            $week = $obj->weekNumber($operationDate);
            $obj->makeCount($week);
            if ($operationCurrency != 'EUR') {
                $currencyRate = $obj->currencyConvert($operationCurrency);
                $operationAmount = round(($operationAmount / $currencyRate), 2);
            }
            if ($operationAmount <= 1000 && $_SESSION['count'] < 3) {
                $commission = 0;
            } elseif ($operationAmount > 1000) {
                $commission = round((($operationAmount - 1000) * 0.3 / 100), 2);
                if ($operationCurrency != 'EUR') {
                    $commission = round(($commission * $currencyRate), 2);
                }
            } else {
                $commission = round(($operationAmount * 0.3 / 100), 2);
                if ($operationCurrency != 'EUR') {
                    $commission = round(($commission * $currencyRate), 2);
                }
            }
        }
        return $commission;
    }

    /**
     * Return week number of operation date.
     *
     * @return integer
     */
    public function weekNumber($operationDate) {
        $operationDate = explode('-', $operationDate);
        $month = $operationDate[1];
        $day = $operationDate[2];
        $year = $operationDate[0];
        $weekNumber = strftime('%W', mktime(0, 0, 0, (int) $month, (int) $day, (int) $year));
        $weekNumber += 0;
        $firstDayOfYear = getdate(mktime(0, 0, 0, 1, 1, (int) $year));
        if ($firstDayOfYear['wday'] != 1) {
            ++$weekNumber;
        }
        return $weekNumber;
    }

    /**
     * Update session values based on week parameter received.
     *
     * @return null
     */
    public function makeCount($week) {
        if ($_SESSION['week'] == $week) {
            $_SESSION['count'] = (int) $_SESSION['count'] + 1;
        } else {
            $_SESSION['count'] = 0;
            $_SESSION['week'] = $week;
        }
    }

    /**
     * Convert currency rate when currency is not EUR.
     *
     * @return float
     */
    public function currencyConvert($operationCurrency) {
        $req_url = 'http://api.exchangeratesapi.io/v1/latest?access_key=0f43e30053ec4716b38b3794c92ac482&symbols='.$operationCurrency;
        $response_json = file_get_contents($req_url);
        $response = json_decode($response_json, true);
        $currencyRate = $response['rates'][$operationCurrency];
        return $currencyRate;
    }
}