<?php
require_once 'src/Commission/Calculation.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $newUpload = new Calculation();
    $newUpload->calculate();
    exit();
}
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
    <label for="file">Upload CSV: </label>
    <input type="file" name="file" /> <br/>
    <input type="submit" name="importSubmit" value="IMPORT" />
</form>