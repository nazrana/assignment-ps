<?php

ini_set('display_errors', '1');
error_reporting(E_ALL | E_STRICT);

session_start();

function template($filename) {
    ob_start();
    include $filename;
    $content = ob_get_clean();

    ob_start();
    include 'base.html.php';
    return ob_get_clean();
}